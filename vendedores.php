<!doctype html>
<html lang="es">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
        <!-- jQuery UI -->
        <link rel="stylesheet" href="assets/js/jquery-ui-1.12.1/jquery-ui.min.css">

        <!-- DataTables -->
        <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

        <link rel="stylesheet" href="assets/css/estilos.css">

        <title>eShoppingTrac - Seguimiento de compras online</title>
    </head>
    <body>
<?php 
require_once("config/database.php");
?>        
        <nav class="navbar navbar-expand-md navbar-dark bg-dark ">
            <a class="navbar-brand" href="#">eT <span class="sr-only">(current)</span></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="index.php">Inicio </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="compras_listado.php">Listado </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="compras.php">Añadir</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="productos.php">Productos</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="https://example.com" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Admin</a>
                        <div class="dropdown-menu" aria-labelledby="dropdown01">
                            <a class="dropdown-item" href="productos.php">Productos</a>
                            <a class="dropdown-item" href="marcas.php">Marcas</a>
                            <a class="dropdown-item" href="modelos.php">Modelos</a>
                            <a class="dropdown-item" href="tiendas.php">Tiendas</a>
                            <a class="dropdown-item" href="vendedores.php">Vendedores</a>
                            <a class="dropdown-item" href="compradores.php">Compradores</a>
                        </div>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                </form>
            </div>
        </nav>
        <div class="container">
            <div class="row">
                <div class="col-md-10 offset-md-1">
                    <div id="resultado-vendedores">
                    </div>
                    
                </div>
             </div>
             <div class="row">
                <div class="col-md-10 offset-md-1">
                    <h2>Listado de vendedores</h2>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add_vendedor">Añadir 
                        <i class="fas fa-plus-square"></i>
                    </button>
                    <div id="listado">
                        <table class="table table-striped" id="vendedores-listado">
                            <thead>
                                <tr>
                                    <th>Acción</th>
                                    <th>Nombre</th>
                                    <th>Tienda</th>
                                    <th>Escaparate</th>
                                    <th>Valoración</th>
                                </tr>
                            </thead>
                            <tbody>
<?php 
$vendedores_sql = "
    SELECT v.id, 
           v.nombre, 
           v.url, 
           t.nombre as tienda, 
           v.valoracion, 
           valoraciones.nombre as valoracion_tipo
    FROM vendedores v
    INNER JOIN tiendas t
       ON v.tienda_id = t.id
    INNER JOIN valoraciones
       ON v.valoracion_id = valoraciones.id
    ORDER BY 2;
";

$tiendas_sql = "
    SELECT id, nombre
    FROM tiendas
    ORDER BY 2";

$valoraciones_sql = "
    SELECT id, nombre
    FROM valoraciones
    ORDER BY 1";

$vendedores = mysqli_query($conn, $vendedores_sql);


while ($vendedor = mysqli_fetch_array($vendedores)) {
    if ($vendedor["valoracion_tipo"] == "Porcentaje") {
        $valoracion = number_format($vendedor["valoracion"], 1, ',', '') . " %";
    } else {
        $valoracion = number_format($vendedor["valoracion"], 1, ',','') . " / 5";
    }
    echo "
                                <tr>
                                    <td>
                                        <a data-id='" . $vendedor["id"] . "' type='button' class='btn btn-danger vendedor_id' data-toggle='modal' data-target='#del_vendedor'> 
                                            <i class='fas fa-trash-alt'></i>
                                        </a>
                                        <a type='button' class='btn btn-secondary' data-toggle='modal' data-target='#edit_vendedor'>
                                            <i class='fas fa-edit'></i>
                                        </a></td>
                                    <td>" . $vendedor["nombre"] . "</td>
                                    <td>" . $vendedor["tienda"] . "</td>
                                    <td><a href='" . $vendedor["url"] . "'>" . $vendedor["url"] . "</td>
                                    <td>" . $valoracion . "</td>
                                </tr>";
}
?>                            
                            </tbody>
                        </table>
                    </div> <!-- listado -->
                </div>
            </div>
        </div>
        <!-- Añadir producto -->
        <div class="modal fade" id="add_vendedor" tabindex="-1" role="dialog" aria-labelledby="add_vendedor" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Nuevo vendedor</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form id="form-vendedores">
                            <div class="row">
                                <div class="col-md-12 mb-3">
                                    <label for="nombre">Nombre</label>
                                    <input type="text" class="form-control" name="nombre" id="nombre" required>
                                </div>
                                <div class="col-md-12 mb-3">
                                    <label for="apellidos">Tienda</label>
                                    <select class="custom-select" name="tienda_id" id="tienda_id">
                                        <option value="">Selecciona...</option>
<?php 
$tiendas = mysqli_query($conn, $tiendas_sql);
while ($tienda = mysqli_fetch_array($tiendas)) {
    echo " 
                                        <option value='" . $tienda["id"] . "'>" . $tienda["nombre"] . "</option>";
}
?>                                        
                                    </select>
                                </div>
                                <div class="col-md-12 mb-3">
                                    <label for="url">Enlace a la web del vendedor</label>
                                    <input type="text" class="form-control" name="url" id="url" required>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="valoracion">Valoración</label>
                                    <input type="text" class="form-control" name="valoracion" id="valoracion" required>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="valoracion_id">Tipo de valoración</label>
                                    <select class="custom-select" name="valoracion_id" id="valoracion_id">
                                        <option value="">Selecciona...</option>
<?php 
$tipos_valoracion = mysqli_query($conn, $valoraciones_sql);
while ($tipo_valoracion = mysqli_fetch_array($tipos_valoracion)) {
    echo " 
                                        <option value='" . $tipo_valoracion["id"] . "'>" . $tipo_valoracion["nombre"] . "</option>";
}
?>                                        
                                    </select>
                                </div>
                            </div>
                            <hr class="mb-4">
                            <button class="btn btn-primary btn-lg btn-block" type="button" onclick="crear_vendedor();">Añadir</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- modal -->
        <!-- Modificar vendedor -->
        <div class="modal fade" id="edit_vendedor" tabindex="-1" role="dialog" aria-labelledby="edit_vendedor" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Modificar vendedor</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form id="form-vendedores">
                            <div class="row">
                                <div class="col-md-12 mb-3">
                                    <label for="nombre">Nombre</label>
                                    <input type="text" class="form-control" name="nombre" id="nombre" required>
                                </div>
                                <div class="col-md-12 mb-3">
                                    <label for="apellidos">Tienda</label>
                                    <select class="custom-select" name="tienda_id" id="tienda_id">
                                        <option value="">Selecciona...</option>
<?php 
$tiendas = mysqli_query($conn, $tiendas_sql);
while ($tienda = mysqli_fetch_array($tiendas)) {
    echo " 
                                        <option value='" . $tienda["id"] . "'>" . $tienda["nombre"] . "</option>";
}
?>                                        
                                    </select>
                                </div>
                                <div class="col-md-12 mb-3">
                                    <label for="url">Enlace a la web del vendedor</label>
                                    <input type="text" class="form-control" name="url" id="url" required>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="valoracion">Valoración</label>
                                    <input type="text" class="form-control" name="valoracion" id="valoracion" required>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="valoracion_id">Tipo de valoración</label>
                                    <select class="custom-select" name="valoracion_id" id="valoracion_id">
                                        <option value="">Selecciona...</option>
<?php 
$tipos_valoracion = mysqli_query($conn, $valoraciones_sql);
while ($tipo_valoracion = mysqli_fetch_array($tipos_valoracion)) {
    echo " 
                                        <option value='" . $tipo_valoracion["id"] . "'>" . $tipo_valoracion["nombre"] . "</option>";
}
?>                                        
                                    </select>
                                </div>
                            </div>
                            <hr class="mb-4">
                            <button class="btn btn-primary btn-lg btn-block" type="button" onclick="crear_vendedor();">Añadir</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- modal -->

        <!-- Eliminar vendedor -->
        <div class="modal fade" id="del_vendedor" tabindex="-1" role="dialog" aria-labelledby="del_vendedor" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Eliminar vendedor</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>¿Estás seguro de eliminar este vendedor: <span id='vendedor_id'></span></p>
                        <form id="form-vendedores">
                            <div class="row">
                                
                            </div>
                            <hr class="mb-4">
                            <button class="btn btn-danger btn-lg btn-block" type="button" onclick="eliminar_vendedor();">Eliminar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- modal -->

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="assets/js/jquery-ui-1.12.1/external/jquery/jquery.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
        <!-- jQuery UI -->
        <script src="assets/js/jquery-ui-1.12.1/jquery-ui.min.js"></script>

        <!-- DataTables -->
        <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script>
            $(document).ready( function () {
                $('#vendedores-listado').DataTable({
                    language: {
                        "sProcessing":     "Procesando...",
                        "sLengthMenu":     "Mostrar _MENU_ registros",
                        "sZeroRecords":    "No se encontraron resultados",
                        "sEmptyTable":     "Ningún dato disponible en esta tabla",
                        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix":    "",
                        "sSearch":         "Buscar:",
                        "sUrl":            "",
                        "sInfoThousands":  ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst":    "Primero",
                            "sLast":     "Último",
                            "sNext":     "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    }
                });
            } );
        </script>

        <!-- Scripts personalizados -->
        <script src="assets/js/scripts.js"></script>
        <script type="text/javascript">
            $(document).on("click", ".vendedor_id", function () {
                var vendedor_id = $(this).data('id');
                $(".modal-body #vendedor_id").html(vendedor_id);
                $('#del_vendedor').modal('show');
            });
        </script>
        <script>
            $("#fecha_compra").datepicker({
                firstDay: 1,
                dateFormat: "yy-mm-dd",
                closeText: "Cerrar",
                prevText: "&#x3C;Ant",
                nextText: "Sig&#x3E;",
                currentText: "Hoy",
                monthNames: [ "enero","febrero","marzo","abril","mayo","junio", "julio","agosto","septiembre","octubre","noviembre","diciembre" ],
                dayNames: [ "domingo","lunes","martes","miércoles","jueves","viernes","sábado" ],
                dayNamesShort: [ "dom","lun","mar","mié","jue","vie","sáb" ],
                dayNamesMin: [ "D","L","M","X","J","V","S" ]
            });
        </script>
    </body>
</html>