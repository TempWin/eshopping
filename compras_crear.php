<?php
ini_set('display_errors', 'On');
require_once("config.php");

if (isset($_POST["descripcion"])) {
    $descripcion = $_POST["descripcion"];
}

if (isset($_POST["descripcion_original"])) {
    $descripcion_original = $_POST["descripcion_original"];
}

if (isset($_POST["url"])) {
    $url = $_POST["url"];
}

if (isset($_POST["comprador_id"])) {
    $comprador_id = $_POST["comprador_id"];
}

if (isset($_POST["tienda_id"])) {
    $tienda_id = $_POST["tienda_id"];
}

if (isset($_POST["producto_id"])) {
    $producto_id = $_POST["producto_id"];
}

if (isset($_POST["vendedor_id"]) && $_POST["vendedor_id"] != "") {
    $vendedor_id = $_POST["vendedor_id"];
} else {
    $vendedor_id = NULL;
}

if (isset($_POST["precio"])) {
    $precio = $_POST["precio"];
}

if (isset($_POST["divisa_id"])) {
    $divisa_id = $_POST["divisa_id"];
}

if (isset($_POST["precio_eur"])) {
    $precio_eur = $_POST["precio_eur"];
}

if (isset($_POST["gastos_envio"])) {
    $gastos_envio = $_POST["gastos_envio"];
}

if (isset($_POST["fecha_compra"])) {
    $fecha_compra = $_POST["fecha_compra"];
}

if (isset($_POST["valoracion"]) && $_POST["valoracion"] != "") {
    $valoracion = $_POST["valoracion"];
} else {
    $valoracion = NULL;
}

if (isset($_POST["valoracion_id"]) && $_POST["valoracion_id"] != "") {
    $valoracion_id = $_POST["valoracion_id"];
} else {
    $valoracion_id = NULL;
}

$insertar_compra_sql = "
    INSERT INTO compras (
            descripcion, 
            descripcion_original, 
            url, 
            comprador_id,
            tienda_id,
            producto_id,
            vendedor_id,
            precio,
            divisa_id,
            precio_eur,
            gastos_envio,
            fecha_compra,
            valoracion,
            valoracion_id
        ) 
        VALUES (
            '$descripcion', 
            '$descripcion_original', 
            '$url', 
            $comprador_id,
            $tienda_id,
            $producto_id,
            $vendedor_id,
            $precio,
            $divisa_id,
            $precio_eur,
            $gastos_envio,
            '$fecha_compra',
            $valoracion,
            $valoracion_id
        )";

if (!mysqli_query($conn, $insertar_compra_sql)) {
    //echo "<p>ERROR en la consulta</p>";
    printf("Error: %s\n", mysqli_error($conn));
} else {
?>
<div class='alert alert-success' role='alert'>Compra añadida</div>
<?php 
}
?>