<!doctype html>
<html lang="es">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
        <!-- jQuery UI -->
        <link rel="stylesheet" href="../assets/js/jquery-ui-1.12.1/jquery-ui.min.css">

        <!-- Estilos personalizados -->
        <link rel="stylesheet" href="assets/css/estilos.css">

        <title>A la rica compra!</title>
    </head>
    <body>
<?php 
require_once("config/database.php");
?>        
        <nav class="navbar navbar-expand-md navbar-dark bg-dark ">
            <a class="navbar-brand" href="#">eT <span class="sr-only">(current)</span></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="index.php">Inicio </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="compras_listado.php">Listado </a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="compras.php">Añadir</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="app/productos.php">Productos</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="https://example.com" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Admin</a>
                        <div class="dropdown-menu" aria-labelledby="dropdown01">
                            <a class="dropdown-item" href="app/productos.php">Productos</a>
                            <a class="dropdown-item" href="app/marcas.php">Marcas</a>
                            <a class="dropdown-item" href="app/modelos.php">Modelos</a>
                            <a class="dropdown-item" href="app/tiendas.php">Tiendas</a>
                            <a class="dropdown-item" href="app/vendedores.php">Vendedores</a>
                            <a class="dropdown-item" href="app/compradores.php">Compradores</a>
                        </div>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                </form>
            </div>
        </nav>
        <div class="container">
            <div class="row">
                <div class="col-md-10 offset-md-1">
                    <!-- Resultado de la compra -->
                    <div id="resultado">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-10 offset-md-1">
                    <h2>Nueva compra</h2>
                    <form id="form-compras">
                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <label for="producto_id">Producto</label>
                                <select class="custom-select d-block w-100" name="producto_id" id="producto_id" required>
                                    <option value="">Elige...</option>
<?php
$productos_sql = "
    SELECT id,
           nombre
    FROM productos
    ORDER BY 2
";

$productos = mysqli_query($conn, $productos_sql);
while ($producto = mysqli_fetch_array($productos)) {
    echo "
                                    <option value='" . $producto["id"] . "'>" . $producto["nombre"] . "</option>";
}
?>                              
                                </select>
                            </div>
                            <div class="col-md-12 mb-3">
                                <label for="descripcion">Descripción</label>
                                <input type="text" class="form-control" name="descripcion" id="descripcion" required>
                            </div>
                            <div class="col-md-12 mb-3">
                                <label for="descripcion_original">Descripción en la tienda</label>
                                <input type="text" class="form-control" name="descripcion_original" id="descripcion_original" required>
                            </div>
                            <div class="col-md-12 mb-3">
                                <label for="url">Enlace compra</label>
                                <input type="text" class="form-control" name="url" id="url" placeholder="" value="" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 mb-3">
                                <label for="fecha_compra">Fecha compra</label>
                                <input type="text" class="form-control" name="fecha_compra" id="fecha_compra" placeholder="" value="" required>
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="comprador_id">Comprador</label>
                                <select class="custom-select d-block w-100" name="comprador_id" id="comprador_id" required>
                                    <option value="">Elige...</option>
<?php
$compradores_sql = "
    SELECT id,
           nombre
    FROM compradores
    ORDER BY 2
";

$compradores = mysqli_query($conn, $compradores_sql);
while ($comprador = mysqli_fetch_array($compradores)) {
    echo "
                                    <option value='" . $comprador["id"] . "'>" . $comprador["nombre"] . "</option>";
}
?>                              
                                </select>
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="tienda_id">Tienda</label>
                                <select class="custom-select d-block w-100" name="tienda_id" id="tienda_id" required>
                                    <option value="">Elige...</option>
<?php
$tiendas_sql = "
    SELECT id, nombre
    FROM tiendas
";

$tiendas = mysqli_query($conn, $tiendas_sql);
while ($tienda = mysqli_fetch_array($tiendas)) {
    echo "
                                    <option value='" . $tienda["id"] . "'>" . $tienda["nombre"] . "</option>";
}
?>
                                </select>
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="vendedor_id">Vendedor</label>
                                <select class="custom-select d-block w-100" name="vendedor_id" id="vendedor_id">
                                    <option value="">Elige...</option>
<?php
$vendedores_sql = "
    SELECT v.id as vendedor_id, 
           v.nombre as vendedor_nombre, 
           t.nombre as tienda_nombre
    FROM vendedores v
    INNER JOIN tiendas t
       ON v.tienda_id = t.id
";

$vendedores = mysqli_query($conn, $vendedores_sql);
while ($vendedor = mysqli_fetch_array($vendedores)) {
    echo "
                                    <option value='" . $vendedor["vendedor_id"] . "'>" . $vendedor["vendedor_nombre"] . " (" . $vendedor["tienda_nombre"] . ")</option>";
}
?>                              
                                </select>
                            </div>
                        </div>
                        <!-- Coste -->
                        <div class="row">
                            <div class="col-md-2 mb-3">
                                <label for="precio">Precio (original)</label>
                                <input type="text" class="form-control" name="precio" id="precio" required>
                            </div>
                            <div class="col-md-2 mb-3">
                                <label for="divisa_id">Moneda</label>
                                <select class="custom-select d-block w-100" name="divisa_id" id="divisa_id" required>
                                    <option value="">Elige...</option>
<?php
$divisas_sql = "
    SELECT id,
           nombre, 
           codigo
    FROM divisas
    ORDER BY 3
";

$divisas = mysqli_query($conn, $divisas_sql);
while ($divisa = mysqli_fetch_array($divisas)) {
    echo "
                                    <option value='" . $divisa["id"] . "'>" . $divisa["codigo"] . " (" . $divisa["nombre"] . ")</option>";
}
?>                              
                                </select>
                            </div>
                            <div class="col-md-2 mb-3">
                                <label for="gastos_envio">Gastos de envío</label>
                                <input type="text" class="form-control" name="gastos_envio" id="gastos_envio" required>
                            </div>
                            <div class="col-md-2 mb-3">
                                <label for="precio_eur">Precio total (€)</label>
                                <input type="text" class="form-control" name="precio_eur" id="precio_eur" required>
                            </div>
                            <div class="col-md-2 mb-3">
                                <label for="valoracion">Valoración</label>
                                <input type="text" class="form-control" name="valoracion" id="valoracion" required>
                            </div>
                            <div class="col-md-2 mb-3">
                                <label for="valoracion_id">Tipo valoración</label>
                                <select class="custom-select d-block w-100" name="valoracion_id" id="valoracion_id" required>
                                    <option value="">Elige...</option>
<?php
$valoraciones_sql = "
    SELECT id,
           nombre
    FROM valoraciones
    ORDER BY 2
";

$valoraciones = mysqli_query($conn, $valoraciones_sql);
while ($valoracion = mysqli_fetch_array($valoraciones)) {
    echo "
                                    <option value='" . $valoracion["id"] . "'>" . $valoracion["nombre"] . "</option>";
}
?>                              
                                </select>
                            </div>
                        </div>

                        <hr class="mb-4">
                        <button class="btn btn-primary btn-lg btn-block" type="button" onclick="crear_compra();">Añadir</button>
                    </form>
                    
                </div>
             </div>
        </div>
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
        <!-- jQuery UI -->
        <script src="../assets/js/jquery-ui-1.12.1/external/jquery/jquery.js"></script>
        <script src="../assets/js/jquery-ui-1.12.1/jquery-ui.min.js"></script>
        <script src="../assets/js/scripts.js"></script>
        <script>
            $("#fecha_compra").datepicker({
                firstDay: 1,
                dateFormat: "yy-mm-dd",
                closeText: "Cerrar",
                prevText: "&#x3C;Ant",
                nextText: "Sig&#x3E;",
                currentText: "Hoy",
                monthNames: [ "enero","febrero","marzo","abril","mayo","junio", "julio","agosto","septiembre","octubre","noviembre","diciembre" ],
                dayNames: [ "domingo","lunes","martes","miércoles","jueves","viernes","sábado" ],
                dayNamesShort: [ "dom","lun","mar","mié","jue","vie","sáb" ],
                dayNamesMin: [ "D","L","M","X","J","V","S" ]
            });
        </script>
    </body>
</html>