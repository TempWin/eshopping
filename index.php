<?php 
error_reporting(E_ALL);
ini_set('display_errors', 1);
?>
<!doctype html>
<html lang="es">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">

        <!-- Estilos personalizados -->
        <link rel="stylesheet" href="assets/css/estilos.css">

        <title>A la rica compra!</title>
    </head>
    <body>
<?php 
require_once("config/database.php");
?>        
        <nav class="navbar navbar-expand-md navbar-dark bg-dark ">
            <a class="navbar-brand" href="#">eT <span class="sr-only">(current)</span></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="index.php">Inicio </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="compras_listado.php">Listado </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="compras.php">Añadir</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="productos.php">Productos</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="https://example.com" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Admin</a>
                        <div class="dropdown-menu" aria-labelledby="dropdown01">
                            <a class="dropdown-item" href="productos.php">Productos</a>
                            <a class="dropdown-item" href="marcas.php">Marcas</a>
                            <a class="dropdown-item" href="modelos.php">Modelos</a>
                            <a class="dropdown-item" href="tiendas.php">Tiendas</a>
                            <a class="dropdown-item" href="vendedores.php">Vendedores</a>
                            <a class="dropdown-item" href="compradores.php">Compradores</a>
                        </div>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                </form>
            </div>
        </nav>
        <div class="container">
            <div class="row">
                <div class="col">
                    <h2>Últimas compras</h2>
                    <table class="table table-responsive table-striped">
                        <thead>
                            <tr>
                                <th>Producto</th>
                                <th>Precio</th>
                                <th>Fecha compra</th>
                            </tr>
                        </thead>
                        <tbody>
<?php
$compras_sql = "
    SELECT c.descripcion, 
           comp.nombre AS comprador, 
           precio_eur AS precio, 
           c.fecha_compra,
           c.url
    FROM compras c
    INNER JOIN compradores comp
       ON c.comprador_id = comp.id
    ORDER BY 4 DESC
    LIMIT 5
";

$compras = mysqli_query($conn, $compras_sql);
while ($compra = mysqli_fetch_array($compras)) {
    echo "
                            <tr>
                                <td><a href='" . $compra["url"] . "'>" . $compra["descripcion"] . "</a></td>
                                <td>" . number_format($compra["precio"], 2, ',','') . "€</td>
                                <td>" . $compra["fecha_compra"] . "</td>
                            </tr>";
}
?>                            
                        </tbody>
                    </table>
                </div>
                <div class="col">
                    <h2>Gastos</h2>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Comprador</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
 <?php
$gastos_2018_sql = "
    SELECT u.nombre, 
       SUM(c.precio_eur + c.gastos_envio) as total
    FROM compras c
    INNER JOIN compradores u
       ON c.comprador_id = u.id
    WHERE YEAR(c.fecha_compra) = 2018
    GROUP BY c.comprador_id
    ORDER BY 1
";

$gastos_2018 = mysqli_query($conn, $gastos_2018_sql);
while ($gastos = mysqli_fetch_array($gastos_2018)) {
    echo "
                        <tr>
                                <td>" . $gastos["nombre"] . "</td>
                                <td>" . number_format($gastos["total"], 2, ',','') . "€</td>
                        </tr>";
}
?>

                        </tbody>
                    </table>
                </div>
                <div class="col">
                    <h2>Pendiente</h2>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Producto</th>
                                <th>Fecha compra</th>
                                <th>Días</th>
                            </tr>
                        </thead>
                        <tbody>
<?php
$compras_pendiente_llegada_sql = "
    SELECT c.descripcion, 
           c.fecha_compra, 
           DATEDIFF(NOW(), c.fecha_envio) as dias
    FROM compras c
    WHERE c.fecha_recepcion IS NULL
      AND DATEDIFF(NOW(), c.fecha_envio) > 0
";

$compras_pendiente_llegada = mysqli_query($conn, $compras_pendiente_llegada_sql);
while ($compra = mysqli_fetch_array($compras_pendiente_llegada)) {
    echo "
                            <tr>
                                <td>" . $compra["descripcion"] . "</td>
                                <td>" . $compra["fecha_compra"] . "</td>";
    if ($compra["dias"] >= 30 && $compra["dias"] <= 60) {
        echo "
                                <td><span class='badge badge-warning'>" . $compra["dias"] . "</span></td>";
    } else if ($compra["dias"] > 60) {
        echo "
                                <td><span class='badge badge-danger'>" . $compra["dias"] . "</span></td>";
    } else {
        echo "
                                <td>" . $compra["dias"] . "</td>";
    }
    echo "
                            </tr>";
}
?>                            
                        </tbody>
                    </table>
                </div>
             </div> <!-- row -->
        </div>
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
    </body>
</html>