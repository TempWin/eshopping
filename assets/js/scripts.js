function crear_producto() {
    var datos_form = $("#form-productos").serialize();

    $.ajax({
        type: 'post',
        url: '../app/productos_crear.php',
        data: datos_form,
        beforeSend: function() {
            console.log("Antes");
        },
        success: function(response) {
            console.log("Bien!");
        },
        error: function() {
            console.log("ERROR");
        }
    });
}

function crear_marca() {
    var datos_form = $("#form-marcas").serialize();

    $.ajax({
        type: 'post',
        url: '../app/marcas_crear.php',
        data: datos_form,
        beforeSend: function() {
            console.log("Antes");
        },
        success: function(response) {
            $("#add_marca").modal('toggle');
            // $("#resultado-marcas").html(response);
            // Recargamos la página para que se muestre la nueva marca
            location.reload();

            //console.log("Marca creada!");
        },
        error: function() {
            console.log("ERROR");
        }
    });
}

function crear_modelo() {
    var datos_form = $("#form-modelos").serialize();

    $.ajax({
        type: 'post',
        url: '../app/modelos_crear.php',
        data: datos_form,
        beforeSend: function() {
            console.log("Antes");
        },
        success: function(response) {
            $("#add_modelo").modal('toggle');
            // $("#resultado-marcas").html(response);
            // Recargamos la página para que se muestre la nueva marca
            location.reload();

            //console.log("Marca creada!");
        },
        error: function() {
            console.log("ERROR creando modelo");
        }
    });
}

function crear_tienda() {
    var datos_form = $("#form-tiendas").serialize();

    $.ajax({
        type: 'post',
        url: '../app/tiendas_crear.php',
        data: datos_form,   
        beforeSend: function() {
            console.log("Antes");
        },
        success: function(response) {
            $("#add_tienda").modal('toggle');
            // $("#resultado-marcas").html(response);
            // Recargamos la página para que se muestre la nueva marca
            location.reload();
            $(".table ").animate({
                backgroundColor: "#c2efb3"
                //color: "#fff"
            }, 1000 );
            $(".table ").animate({
                backgroundColor: "#fff"
                //color: "#fff"
            }, 1000 );

            //console.log("Marca creada!");
        },
        error: function() {
            console.log("ERROR creando tienda");
        }
    });
}

function crear_comprador() {
    var datos_form = $("#form-compradores").serialize();

    $.ajax({
        type: 'post',
        url: '../app/compradores_crear.php',
        data: datos_form,   
        beforeSend: function() {
            console.log("Antes");
        },
        success: function(response) {
            $("#add_comprador").modal('toggle');
            // $("#resultado-marcas").html(response);
            // Recargamos la página para que se muestre la nueva marca
            location.reload();
            $(".table ").animate({
                backgroundColor: "#c2efb3"
                //color: "#fff"
            }, 1000 );
            $(".table ").animate({
                backgroundColor: "#fff"
                //color: "#fff"
            }, 1000 );

            //console.log("Marca creada!");
        },
        error: function() {
            console.log("ERROR creando comprador");
        }
    });
}

function crear_vendedor() {
    var datos_form = $("#form-vendedores").serialize();

    $.ajax({
        type: 'post',
        url: '../app/vendedores_crear.php',
        data: datos_form,   
        beforeSend: function() {
            console.log("Antes");
        },
        success: function(response) {
            $("#add_vendedor").modal('toggle');
            // $("#resultado-marcas").html(response);
            // Recargamos la página para que se muestre la nueva marca
            location.reload();
            $(".table ").animate({
                backgroundColor: "#c2efb3"
                //color: "#fff"
            }, 1000 );
            $(".table ").animate({
                backgroundColor: "#fff"
                //color: "#fff"
            }, 1000 );

            //console.log("Marca creada!");
        },
        error: function() {
            console.log("ERROR creando vendedor");
        }
    });
}

function crear_compra() {
    var datos_form = $("#form-compras").serialize();

    $.ajax({
        type: 'post',
        url: '../app/compras_crear.php',
        data: datos_form,   
        beforeSend: function() {
            console.log("Antes");
        },
        success: function(response) {
            $("#resultado").html(response)
        },
        error: function() {
            console.log("ERROR creando compra");
        }
    });
}