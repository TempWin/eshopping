<!doctype html>
<html lang="es">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">

        <!-- FontAwesome -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">

        <!-- DataTables -->
        <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

        <!-- Material Icons -->
        <!--
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        -->

        <!-- Estilos personalizados -->
        <link rel="stylesheet" href="assets/css/estilos.css">

        <title>A la rica compra!</title>
    </head>
    <body>
<?php 
require_once("config/database.php");
?>        
        <nav class="navbar navbar-expand-md navbar-dark bg-dark ">
              <a class="navbar-brand" href="#">eT</a>
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>

              <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                <ul class="navbar-nav mr-auto">
                  <li class="nav-item">
                    <a class="nav-link" href="index.php">Inicio </a>
                  </li>
                  <li class="nav-item active">
                    <a class="nav-link" href="compras_listado.php">Listado </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="app/compras.php">Añadir</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="app/productos.php">Productos</a>
                  </li>
                  <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="https://example.com" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Admin</a>
                        <div class="dropdown-menu" aria-labelledby="dropdown01">
                          <a class="dropdown-item" href="app/productos.php">Productos</a>
                          <a class="dropdown-item" href="app/marcas.php">Marcas</a>
                          <a class="dropdown-item" href="app/modelos.php">Modelos</a>
                          <a class="dropdown-item" href="app/tiendas.php">Tiendas</a>
                          <a class="dropdown-item" href="app/vendedores.php">Vendedores</a>
                          <a class="dropdown-item" href="app/compradores.php">Compradores</a>
                        </div>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
                  <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
                  <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                </form>
            </div>
        </nav>
        <div class="container">
            <div class="row">
                <div class="col">
                    <h2>Histórico de compras</h2>
                    <table class="table table-striped" id="compras-listado">
                        <thead>
                            <tr>
                                <th>Producto</th>
                                <th>Precio</th>
                                <th>Fecha compra</th>
                                <th>Comprador</th>
                            </tr>
                        </thead>
                        <tbody>
<?php
$compras_sql = "
    SELECT c.descripcion, 
           comp.nombre AS comprador, 
           precio_eur AS precio, 
           c.fecha_compra,
           c.url
    FROM compras c
    INNER JOIN compradores comp
       ON c.comprador_id = comp.id
    ORDER BY 4 DESC
";

$compras = mysqli_query($conn, $compras_sql);
while ($compra = mysqli_fetch_array($compras)) {
    echo "
                            <tr>
                                <td>" . $compra["descripcion"] . " <a href='" . $compra["url"] . "' title='Enlace a la compra'><i class='fa fa-external-link-alt'></i></a></td>
                                <td>" . number_format($compra["precio"], 2, ',','') . "€</td>
                                <td>" . $compra["fecha_compra"] . "</td>
                                <td>" . $compra["comprador"] . "</td>
                            </tr>";
}
?>                            
                        </tbody>
                    </table>
                </div>
             </div> <!-- row -->
        </div>
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>

        <!-- DataTables -->
        <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script>
            $(document).ready( function () {
                $('#compras-listado').DataTable({
                    language: {
                        "sProcessing":     "Procesando...",
                        "sLengthMenu":     "Mostrar _MENU_ registros",
                        "sZeroRecords":    "No se encontraron resultados",
                        "sEmptyTable":     "Ningún dato disponible en esta tabla",
                        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix":    "",
                        "sSearch":         "Buscar:",
                        "sUrl":            "",
                        "sInfoThousands":  ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst":    "Primero",
                            "sLast":     "Último",
                            "sNext":     "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    }
                });
            } );
        </script>

        <!-- Ionicons -->
        <!--
        <script src="https://unpkg.com/ionicons@4.2.4/dist/ionicons.js"></script>
    -->
    </body>
</html>